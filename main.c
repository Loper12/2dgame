/*
 * Main.c
 *
 *  Created on: Mar 24, 2020
 *      Author: finian
 *
 * Handles all main functionality
 */

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdio.h>

GLFWwindow* window;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);

void init() {
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	#endif
	window = glfwCreateWindow(800, 600, "2DGame", NULL, NULL);
	if (window == NULL){
		printf("Failed to create a window\n");
		glfwTerminate();
		return 1;
	} 
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		printf("Failed to load OpenGL");
		glfwTerminate();
		return -1;
	}
}

int main() {
	init();
	return 0;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
	
}
