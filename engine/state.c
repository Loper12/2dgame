/*
 * state.c
 *
 *  Created on: Mar 24, 2020
 *      Author: finian
 */

#include "state.h"
#include <GLFW/glfw3.h>
#include <stdio.h>

typedef struct Gamestate {
	const char *name;
	int (*init)(GLFWwindow*);
	int (*process)(GLFWwindow*);
	int (*render)(GLFWwindow*);
	int (*deinit)(GLFWwindow*);
	int (*size_callback)(GLFWwindow*, int, int);
} Gamestate_t;

Gamestate_t createGameState() {
	Gamestate_t newGameState;
	return newGameState
}

void init(Gamestate_t state, GLFWwindow* window) {
	if(state.init != NULL) {
		state.init(window);
	} else {
		printf("WARNING: Gamestate \"%s\" has no initialization function, but something tried to call it.\n", state.name);
	}
}

void process(Gamestate_t state, GLFWwindow* window) {
	if(state.process != NULL) {
		state.process(window);
	} else {
		printf("WARNING: Gamestate \"%s\" has no processing function, but something tried to call it.\n", state.name);
	}
}

void render(Gamestate_t state, GLFWwindow* window) {
	if(state.render != NULL) {
		state.render(window);
	} else {
		printf("WARNING: Gamestate \"%s\" has no rendering function, (lol why) but something tried to call it.", state.name);
	}
}

void deinit(Gamestate_t state, GLFWwindow* window) {
	if(state.deinit != NULL) {
		state.deinit(window);
	} else {
		printf("WARNING: Gamestate \"%s\" has no deinitialization function, but something tried to call it.", state.name);
	}
}

void size_callback(Gamestate_t state, GLFWwindow* window, int width, int height) {
	if(state.size_callback != NULL) {
		state.size_callback(window, width, height);
	} else {
		printf("Note: Gamestate \"%s\" has no size callback function, but something tried to call it.", state.name);
	}
}
