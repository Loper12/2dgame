/*
 * shader.c
 *
 *  Created on: Mar 24, 2020
 *      Author: finian
 */

#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>

typedef struct shader {
	const char* name; // The shader's name
	const char* code; // The shader code
	unsigned int ID; // The ID
	GLenum type;

	int success; // Whether compilation succeded.
	char infoLog[512]; // A log.
} shader;

/*
 * \brief Struct representing a shader program.
 *
 * \details This struct is a shader program.
 *
 */

typedef struct program {
	const char* name; // The name of the program

	// shaders
	shader vertex; // The vertex shader
	shader fragment; // The fragment shader
	
	// program
	unsigned int ID; // The ID
} program;

shader* createShaderFromText(const char* name, const char* code, GLenum type) {
	shader* shader;
	shader->name = name;
	shader->code = code;
	shader->type = type;
	return shader;
}

shader* createShaderFromFile(const char* name, const char* filename, GLenum type) {
	FILE* fp = fopen(filename, "rb");
	fseek(fp, 0, SEEK_END);
	long fsize = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	char* code = malloc(fsize + 1);
	fread(code, 1, fsize, fp );
	fclose(fp);

	return createShaderFromText(name, code, type);
}

void compile(shader* shader) {
	shader->ID = glCreateShader(shader->type);
	glShaderSource(shader->ID, 1, &(shader->code), NULL);
	glCompileShader(shader->ID);
	
	glGetShaderiv(shader->ID, GL_COMPILE_STATUS, &(shader->success));
	if(!(shader->success)) {
		glGetProgramInfoLog(shader->ID, 512, NULL, shader->infoLog);
		printf("Error: Shader compilation failed. Error: \"%s\". Code:\"%s\"", shader->infoLog, shader->code);
	}
}



