/*
 * state.h
 *
 *  Created on: Mar 24, 2020
 *      Author: finian
 */

#include <GLFW/glfw3.h>

#ifndef ENGINE_STATE_H_
#define ENGINE_STATE_H_

typedef struct Gamestate Gamestate_t;

void init(Gamestate_t state, GLFWwindow* window);
void process(Gamestate_t state, GLFWwindow* window);
void render(Gamestate_t state, GLFWwindow* window);
void deinit(Gamestate_t state, GLFWwindow* window);
void size_callback(Gamestate_t state, GLFWwindow* window, int width, int height);

#endif /* ENGINE_STATE_H_ */
