# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/finian/Documents/EclipseGit/2DGame/engine/state.c" "/home/finian/Documents/EclipseGit/2DGame/CMakeFiles/2DGame.dir/engine/state.c.o"
  "/home/finian/Documents/EclipseGit/2DGame/main.c" "/home/finian/Documents/EclipseGit/2DGame/CMakeFiles/2DGame.dir/main.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "ext/glfw/include"
  "ext/glad/include"
  "ext/Handmade-Math"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/finian/Documents/EclipseGit/2DGame/ext/glfw/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  "/home/finian/Documents/EclipseGit/2DGame/ext/glad/CMakeFiles/glad.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
